package com.deliveryscheduling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliverySchedulingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeliverySchedulingApplication.class, args);
	}

}
