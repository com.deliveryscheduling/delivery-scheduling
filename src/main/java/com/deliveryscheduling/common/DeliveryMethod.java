package com.deliveryscheduling.common;

public enum DeliveryMethod {
    DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP;
}
