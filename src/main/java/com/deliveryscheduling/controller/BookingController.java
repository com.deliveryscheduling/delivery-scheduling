package com.deliveryscheduling.controller;

import com.deliveryscheduling.dto.BookingDTO;
import com.deliveryscheduling.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bookings")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @GetMapping("/all")
    public ResponseEntity<CollectionModel<EntityModel<BookingDTO>>> getAllBookings() {
        CollectionModel<EntityModel<BookingDTO>> bookings = bookingService.getAllBookings();
        return ResponseEntity.ok(bookings);
    }

    @PostMapping("/book-delivery")
    public ResponseEntity<EntityModel<BookingDTO>> bookDelivery(@RequestBody BookingDTO bookingDTO) {
        EntityModel<BookingDTO> booking = bookingService.bookDelivery(bookingDTO);
        return ResponseEntity.ok(booking);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<BookingDTO>> getBooking(@PathVariable Long id) {
        EntityModel<BookingDTO> booking = bookingService.getBooking(id);
        return ResponseEntity.ok(booking);
    }
}

