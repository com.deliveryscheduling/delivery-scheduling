package com.deliveryscheduling.dto;

import com.deliveryscheduling.common.DeliveryMethod;
import com.deliveryscheduling.model.TimeSlot;
import lombok.Data;

import java.time.LocalDate;

@Data
public class BookingDTO {
    private String customerId;
    private DeliveryMethod deliveryMethod;
    private LocalDate deliveryDay;
    private TimeSlot timeSlot;
}

