package com.deliveryscheduling.model;

import com.deliveryscheduling.common.DeliveryMethod;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String customerId;
    @Enumerated(EnumType.STRING)
    private DeliveryMethod deliveryMethod;
    private LocalDate deliveryDay;

    @ManyToOne(fetch = FetchType.LAZY)
    private TimeSlot timeSlot;
}
