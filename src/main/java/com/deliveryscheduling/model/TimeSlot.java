package com.deliveryscheduling.model;

import com.deliveryscheduling.common.DeliveryMethod;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TimeSlot {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long timeSlotId;
    @Enumerated(EnumType.STRING)
    private DeliveryMethod deliveryMethod;
    private LocalDate deliveryDay;
    private boolean isItAvailable;
}

