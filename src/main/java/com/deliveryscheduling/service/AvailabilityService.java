package com.deliveryscheduling.service;

import com.deliveryscheduling.model.TimeSlot;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class AvailabilityService {

    @KafkaListener(topics = "availability-updates")
    public void handleAvailabilityUpdate(TimeSlot timeSlot) {
        // Update time slot availability based on Kafka message
        System.out.println("Received availability update for time slot: " + timeSlot.getTimeSlotId());
        // Perform logic to update availability in database or cache
    }
}
