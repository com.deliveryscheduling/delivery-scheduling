package com.deliveryscheduling.service;

import com.deliveryscheduling.controller.BookingController;
import com.deliveryscheduling.dto.BookingDTO;
import com.deliveryscheduling.model.Booking;
import com.deliveryscheduling.respository.BookingRepository;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookingService {

    @Autowired
    private KafkaTemplate<String, BookingDTO> kafkaTemplate;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private ModelMapper modelMapper;

    public EntityModel<BookingDTO> bookDelivery(BookingDTO bookingDTO) {
        // Business logic to process booking
        kafkaTemplate.send("booking-events", bookingDTO);
        // Create HATEOAS representation for booking
        Booking saved = bookingRepository.save(modelMapper.map(bookingDTO, Booking.class));
        EntityModel<BookingDTO> bookingModel = EntityModel.of(modelMapper.map(saved, BookingDTO.class));
        WebMvcLinkBuilder linkToSelf = WebMvcLinkBuilder
                .linkTo(WebMvcLinkBuilder.methodOn(BookingController.class).bookDelivery(bookingDTO));
        bookingModel.add(linkToSelf.withSelfRel());
        return bookingModel;
    }

    public CollectionModel<EntityModel<BookingDTO>> getAllBookings() {
        List<Booking> bookings = bookingRepository.findAll();
        List<EntityModel<BookingDTO>> bookingModels = bookings.stream()
                .map(entity -> {
                    BookingDTO mapped = modelMapper.map(entity, BookingDTO.class);
                    return EntityModel.of(mapped);
                })
                .collect(Collectors.toList());

        return CollectionModel.of(bookingModels,
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(BookingController.class).getAllBookings()).withSelfRel());
    }

    @SneakyThrows
    public EntityModel<BookingDTO> getBooking(Long id) {
        Optional<Booking> optionalBooking = bookingRepository.findById(id);
        if (optionalBooking.isPresent()) {
            Booking booking = optionalBooking.get();
            BookingDTO bookingDTO = modelMapper.map(booking, BookingDTO.class);
            EntityModel<BookingDTO> model = EntityModel.of(bookingDTO,
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(BookingController.class).getBooking(id)).withSelfRel(),
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(BookingController.class).getAllBookings()).withRel("bookings"));
            return model;
        }

        throw new Exception("Booking not found");
    }

}

