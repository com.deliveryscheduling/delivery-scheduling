package com.deliveryscheduling.service;

import com.deliveryscheduling.dto.BookingDTO;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    @KafkaListener(topics = "booking-events")
    public void handleBookingEvent(BookingDTO bookingDTO) {
        // Send notification to customer about booking confirmation
        System.out.println("Sending notification to customer: Booking confirmed for customer " + bookingDTO.getCustomerId());
        // Perform logic to send notification (e.g., email, SMS)
    }
}

